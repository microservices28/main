# Проектная работа

* https://shop.35.228.78.144.nip.io/ - Развернутый магазин;
![grafana](./docs/img/shop.png)
* https://argocd.35.228.78.144.nip.io -argocd   admin/argocd-server-7fdb567cd4-bjs8f;
![argocd](./docs/img/argocd.png)
* https://grafana.35.228.78.144.nip.io/ - grafana (admin/prom-operator)
![grafana](./docs/img/nginx.png)
* https://kibana.35.228.78.144.nip.io/ - Kibana - сделан dasboard для nginx :
![kibana](./docs/img/kibana.png)
Общий Discovery:
![kibana1](./docs/img/kibana1.png)




# Развертывание кластера

![Развертывание проекта](./docs/img/gitlab+gcp.png)

 Необходимо запустить вручную pipeline ,который состоит из четырех jobs:
![pipeline](./docs/img/pipiline.png)

 
 1)create-cluster:
 * создание кластера GCP из проекта [main-infra](https://gitlab.com/microservices28/main-infra),для этого предварительно нужно создать [service-account](https://cloud.google.com/iam/docs/creating-managing-service-accounts) в google и добавить переменную GCLOUD_SERVICEACCOUNT в env variables:
 `base64 serviceaccount.json`
  * Файл с переменными кластера terraform.tfvars
 ```
project_id = "hypnotic-seat-309810"
region     = "europe-north1"
zone       = "europe-north1-a"
machine_type = "e2-standard-2"
disk_size = "30"
credentials ="./creds/serviceaccount.json"
num_nodes = "4"
```

 * После создания кластера -  привязываем его к группе проекта :
 https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#existing-kubernetes-cluster

2)deploy-basic - установка в кластер экосистемы :
* ingress-nginx 3 ноды с servicemonitor;
* cert-manager;
* elasticsearch,kibana,fluent-bit;
* prometheus + grafana+alertmanager;

3)deploy-argocd:
* Установка argocd;


4)apply-ingress-rules:
* Применение ингрес манифестов с актуальным ip load balancer ;

В группе есть 11 микросервисов [microservices](https://gitlab.com/microservices28),в каждом свой  ci/cd на build и deploy:
* Build происходит в автоматическом режиме при применение тэга.Сборка образа осуществляется в gitlab container registry;
* Deploy запускается  в ручном режиме при наличие тэга в коммите ;
* Frontend и loadgenerator в автоматическом режиме деплоятся с актуальным loadbalance;
* Пример плана (старался максимально унифицировать) :
```
variables:
  NAMESPACE: $CI_PROJECT_ROOT_NAMESPACE
  ENVIRONMENT: "production"


stages:
  - build
  - deploy


build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n  ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64)\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/src/ --dockerfile $CI_PROJECT_DIR/src/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG 
  only:
    - tags

deploy:
  image: dtzar/helm-kubectl:latest
  stage: deploy
  script:
    - export nginx_ingress_ip=$(kubectl get svc --namespace=ingress-nginx ingress-nginx-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
    - helm upgrade -i --create-namespace -n $NAMESPACE -f deploy/charts/$CI_PROJECT_NAME/values.yaml 
      --set image.repository=$CI_REGISTRY_IMAGE
      --set image.tag=$CI_COMMIT_TAG
      --set ingress.host=$nginx_ingress_ip
      $CI_PROJECT_NAME deploy/charts/$CI_PROJECT_NAME
  environment:
    name: $ENVIRONMENT
    kubernetes:
      namespace: $NAMESPACE
  only:
    - tags
  when: manual
```
Также ,помимо ручного деплоя ,развернут argocd ,который мониторит изменения в гите и синхронирует приложения.Для отслеживания обновления образа в registry используется argocd-image-updater - https://argocd-image-updater.readthedocs.io/en/stable/ - используются специальные аннотации приложений argocd :
```
argocd-image-updater.argoproj.io/image-list:
argocd-image-updater.argoproj.io/write-back-method:
```
Фактически argocd-image-updater отслеживает каждые 2 минуты репозитории образов,и ,в случае изменения(увеличения версии semver),делает 
```
argocd app set --helm-set image.tag=v1.0.1
```


## Architecture

**Online Boutique** is composed of 11 microservices written in different
languages that talk to each other over gRPC. See the [Development Principles](/docs/development-principles.md) doc for more information.

[![Architecture of
microservices](./docs/img/architecture-diagram.png)](./docs/img/architecture-diagram.png)


| Service                                              | Language      | Description                                                                                                                       |
| ---------------------------------------------------- | ------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| [frontend](https://gitlab.com/microservices28/frontend)                           | Go            | Exposes an HTTP server to serve the website. Does not require signup/login and generates session IDs for all users automatically. |
| [cartservice](https://gitlab.com/microservices28/cartservice)                     | C#            | Stores the items in the user's shopping cart in Redis and retrieves it.                                                           |
| [productcatalogservice](https://gitlab.com/microservices28/productcatalogservice) | Go            | Provides the list of products from a JSON file and ability to search products and get individual products.                        |
| [currencyservice](https://gitlab.com/microservices28/productcatalogservice)             | Node.js       | Converts one money amount to another currency. Uses real values fetched from European Central Bank. It's the highest QPS service. |
| [paymentservice](https://gitlab.com/microservices28/paymentservice)               | Node.js       | Charges the given credit card info (mock) with the given amount and returns a transaction ID.                                     |
| [shippingservice](https://gitlab.com/microservices28/shippingservice)             | Go            | Gives shipping cost estimates based on the shopping cart. Ships items to the given address (mock)                                 |
| [emailservice](https://gitlab.com/microservices28/emailservice)                   | Python        | Sends users an order confirmation email (mock).                                                                                   |
| [checkoutservice](https://gitlab.com/microservices28/checkoutservice)             | Go            | Retrieves user cart, prepares order and orchestrates the payment, shipping and the email notification.                            |
| [recommendationservice](https://gitlab.com/microservices28/recommendationservice) | Python        | Recommends other products based on what's given in the cart.                                                                      |
| [adservice](https://gitlab.com/microservices28/adservice)                         | Java          | Provides text ads based on given context 
| [loadgenerator](./src/loadgenerator)                 | Python/Locust | Continuously sends requests imitating realistic user shopping flows to the frontend.                        








